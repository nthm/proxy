const https = require('https')
const http = require('http')
const fs = require('fs')
const path = require('path')

// If you need a localhost cert, use `mkcert`
const ssl = {
  key: fs.readFileSync(path.resolve(__dirname, '../kudasai.local-key.pem')),
  cert: fs.readFileSync(path.resolve(__dirname, '../kudasai.local.pem')),
}

const server = https.createServer(ssl, (req, res) => {
  const { method, headers, headers: { host } } = req
  // Default HTTP options that apply to all servers
  const options = {
    method,
    path: req.url,
    headers,
    timeout: 10000,
  }
  // For non-standard ports (like 8443), browsers might put the port in the
  // host header. Take it out here
  const parsedHost = host.split(':', 1)[0]

  // TW Notebooks
  if
  (parsedHost === 'kudasai.local') {
    // Not a proxy
    res.statusCode = 200
    return res.end('Hi')
  }
  else if
  (parsedHost === 'note.kudasai.local' && req.url.startsWith('/1')) {
    options.port = 10801
  }
  else if
  (parsedHost === 'note.kudasai.local' && req.url.startsWith('/2')) {
    options.port = 10802
  }
  else if
  // Syncthing
  (parsedHost === 'sync.kudasai.local') {
    options.port = 8384
    options.headers.host = 'localhost' // Syncthing does a host check
  }
  else {
    res.statusCode = 404
    res.statusMessage = 'No such host'
    return res.end('Nothing to see here')
  }

  const proxyReq = http.request(options)
  setupRequestErrorHandlers({ req, res }, proxyReq)
  req.pipe(proxyReq)

  proxyReq.on('response', proxyRes => {
    // No, you can't just res.headers = proxyRes.headers
    for (const key in proxyRes.headers) {
      res.setHeader(key, proxyRes.headers[key])
    }
    proxyRes.pipe(res)

    console.log('\n')
    console.log('Req:', req.method, req.headers.host + req.url)
    console.log('ProxyReq:', options.method, options.headers.host + ':' + options.port + options.path)
    console.log('ProxyRes:', proxyRes.statusCode, proxyRes.headers['content-type'])
  })
})

const setupRequestErrorHandlers = ({ req, res }, proxyReq) => {
  req.on('error', err => {
    console.error('Client request error; aborting proxy request')
  
    // Stop trying to reach the endpoint for the client if they drop off
    if (req.socket.destroyed && err.code === 'ECONNRESET') {
      proxyReq.abort()
    }
  })
  req.on('aborted', () => {
    proxyReq.abort()
  })
  proxyReq.on('error', err => {
    res.statusCode = 504
    res.end('Error:', err)
  })
  proxyReq.on('timeout', err => {
    res.statusCode = 503
    res.end('Timeout:', err)
  })
}

server.listen(443, () => {
  console.log('Server listening on 0.0.0.0:443')
})
