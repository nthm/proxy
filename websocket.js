/**
 * Websocket upgrades
 *
 * Shows how to handle websocket creation via protocol switch when a client
 * requests the socket upgrade.
 * 
 * Works with HTTP, HTTPS, and HTTP/2
 * 
 * Notice that the .on('upgrade') handler doesn't need to do any host matching
 * because it receives a live request that is already tied to a host
 */

const https = require('https')
const server = https.createServer((req, res) => {
  // Your normal web server routes here...
  res.end()
})

server.on('upgrade', (req, socket, head) => {

  // This is the standard (node-http-proxy) way of recreating a header block.
  // However, it's slow. If you need speed look into injecting capture code to
  // the `http.IncomingMessage.prototype._addHeaderLine()` function

  const createHTTPHeaderString = (line, headers) =>
    Object.entries(headers).reduce((head, [key, value]) => {
      if (!Array.isArray(value)) {
        head.push(key + ': ' + value)
      } else {
        // This is needed because Node parses some headers, like Set-Cookie,
        // into an array - always
        head.push(...value.map(val => key + ': ' + val))
      }
      return head
    }, [line])
      .join('\r\n') + '\r\n\r\n'

  socket.setTimeout(0)
  socket.setNoDelay(true)
  socket.setKeepAlive(true, 0)

  // Note that .unshift() is part of the ReadableStream class
  if (head && head.length) {
    socket.unshift(head)
  }

  // Forward the request from client to target
  // TODO: Copy all the headers, change `Host:` and other fields
  const proxyReq = https.request('localhost:8443')

  proxyReq.on('error', () => {
    socket.end()
  })
  proxyReq.on('response', res => {
    if (!res.upgrade) {
      // Their server said the upgrade isn't happening - close the socket
      const { httpVersion, statusCode, statusMessage } = res
      // Note: If using an HTTP/2 proxy that talks to HTTP/1.1 maybe respond /2
      socket.write(createHTTPHeaderString(`HTTP/${httpVersion} ${statusCode} ${statusMessage}`, res.headers))
      res.pipe(socket)
    }
  })

  proxyReq.on('upgrade', (proxyRes, proxySocket, proxyHead) => {
    proxySocket.on('error', () => {
      socket.end()
    })

    proxySocket.on('end', () => {
      console.log('Closed a websocket')
    })

    socket.on('error', () => {
      proxySocket.end()
    })

    proxySocket.setTimeout(0)
    proxySocket.setNoDelay(true)
    proxySocket.setKeepAlive(true, 0)

    if (proxyHead && proxyHead.length) {
      proxySocket.unshift(proxyHead)
    }

    // Note: Again, use the proper HTTP version for your server
    socket.write(createHTTPHeaderString('HTTP/1.1 101 Switching Protocols', proxyRes.headers))
    proxySocket.pipe(socket).pipe(proxySocket)
  })

  return proxyReq.end()
})

server.listen(443)
