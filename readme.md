# Node reverse proxy cookbook

Suckless reverse proxy cookbook using Node. The standard libraries for Node
have everything you need to create one in HTTP, HTTPS, and/or HTTP/2.

Node can be a nice alternative to Apache/NGINX/Caddy. You don't need to learn
yet another DSL. You can replace headers, change bodies, load balance, anything
you need.

I wouldn't recommend using these examples for anything in production. Instead,
check out Koa.js. It's an abstraction to Node's HTTP and provides a nicer
interface for chaining, middleware, and error handling.

## Demistifying a reverse proxy

Some think of proxies as being very different and requiring special software.
People often consider NGINX as _the_ reverse proxy, and put it infront of other
webservers like Apache. Proxying isn't very special. Here's the gist:

Setup a normal webserver. For each received request stream, start a request to
another server, copy over the headers, and pipe them together. When the response
comes back from the other server, you copy headers and pipe the body it to the
original response.

That's all, really.

You'll likely want to listen for errors, websocket upgrades, protocol
negotiations, ECONNRESET, timeouts... It's up to you. I've posted some other
ideas/features below to consider:

## Ideas

This cookbook has an example on how to setup a normal HTTP reverse proxy as an
HTTPS server in _normal.js_. It's what I use, which isn't much.

It serves two TiddlyWikis and Syncthing.

You should have some kind of looping to go through all your server blocks. Or
use an `if, else if` chain, or `(() => { switch () { case: return } })()`.
Anything works.

I'll post more layouts as I need them. There's a _websocket.js_ too.

You may need more, though:

- Is your frontend served with a path prefix that your backend doesn't know
  about?

```js
proxyReqOptions.path = req.url.slice('/special'.length)
console.log('Rewrote', req.url, 'to', options.path)
```

- Does your proxy redirect elsewhere? Use `follow-redirects` module instead of
  the default `http.Agent`. It's like `curl -L` and written to follows RFC.

- Custom URL modification? Cookies? Rewriting the proxy body? There are many
  good examples of this in the _node-http-proxy_ codebase.

```js
const match = /api-v(\d+)\.example\.com/.match(url)
const version = match[1]
http.request(`http://[::FFFF:129.144.52.38]:3000/api/v${version}`)
```

To rewrite the response, first capture all of the chunks _before_ you pipe it to
the client:

```js
proxyRes.on('response', res => {
  if (!res.finished) {
    // Response not done, but waiting for it
    proxyRes.on('end', () => {
      // OK. Have all the response
      // Do MitM things here... then pipe it back

      res.pipe(proxyRes)
    })
  }
})
```

- CORS? Try adding some headers. Also see the MDN page or modules that
  specialize in this.

```js
res.setHeader('Access-Control-Allow-Origin', '*')
res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
```

- Load balancing? Increment a pointer `servers[i++ % servers.length]`-style.
- Clean ups such as setting `Content-Length` to 0 if request type is `DELETE`
- Proxy timeouts via `req.socket.setTimeout()`

## Rationale

Delegating requests to other servers can be needlessly difficult in web servers.
People spend hours consulting hundreds of pages of documentation when they could
implement the functionality they need in 4 lines of JS.

There are other Node proxies that exist; maybe even for this reason. Two popular
projects are _Redbird_ and _node-http-proxy_. However, they each implement
features beyond what a reverse proxy should be responsible for - such as
LetsEncrypt certificate renewals or Docker integration.

They each have a many configuration options to learn resulting in tedious
documentation. Each has thousands of lines of code not including their
dependency trees. Open tickets and merge requests from users concerned from out
of date dependencies or deprecation warnings are stacking up. Furthermore, it's
not uncommon in the Node ecosystem to find malware in third parties modules.

For something as simple as a reverse proxy, you shouldn't need to trust any
dependencies. The core API has everything needed. Short and properly commented
code is the best documentation.

## Performance considerations for Node

This isn't about performance, it's for developer ease. It's very likely fast
enough for you.
