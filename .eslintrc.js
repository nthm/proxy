module.exports = {
  'extends': [
    'eslint:recommended',
  ],
  'env': {
    'browser': true,
    'node': true,
    // https://github.com/eslint/eslint/issues/9812#issuecomment-355772014
    'es6': true,
  },
  'parserOptions': {
    'ecmaVersion': 8,
    'sourceType': 'module',
    'ecmaFeatures': {
      'experimentalObjectRestSpread': true,
    },
  },
  'rules': {
    'indent': ['error', 2],
    'eol-last': ['error', 'always'],
    'semi': ['error', 'never'],
    'quotes': ['error', 'single', {
      'avoidEscape': true,
      'allowTemplateLiterals': true,
    }],
    'comma-dangle': ['error', {
      'arrays': 'always-multiline',
      'objects': 'always-multiline',
      'imports': 'never',
      'exports': 'never',
      'functions': 'never',
    }],
    'space-before-function-paren': ['error', {
      'anonymous': 'always',
      'named': 'never',
      'asyncArrow': 'always',
    }],
    'no-console': 'off',
    'no-multi-spaces': ['error', {
      'ignoreEOLComments': true,
    }],
    'object-curly-spacing': ['error', 'always'],
  },
}
